#! /bin/bash

sudo add-apt-repository -yu ppa:kdenlive/kdenlive-stable

# Install Additional Applications: 
sudo apt-get install kdenlive -y

# Install OpenTimelineIO (for KdenLive):
sudo apt install python3-pip
python -m pip install opentimelineio
