#! /bin/bash

# Install Fish:
sudo apt-add-repository -yu ppa:fish-shell/release-3
sudo apt install -y fish git fonts-firacode
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
omf install agnoster
omf install bobthefish
omf theme bobthefish
set -g theme_nerd_fonts yes
chsh -s /usr/bin/fish

# Configure:
fish_add_path ~/.local/bin
