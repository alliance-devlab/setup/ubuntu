#! /bin/fish

omf install bass

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

cp ./nvm.fish ~/.config/fish/functions/nvm.fish

nvm install v14 #--lts
nvm use v14 #node
cd /usr/local/bin
sudo ln -s (which node) ./node
sudo ln -s (which npm) ./npm
#fish_add_path (npm list -g | head -1)/../bin
fish_add_path ~/.nvm/versions/node/(node --version)/bin
npm config set prefix ~/.nvm/versions/node/(node --version)
set -Ux NODE_PATH $HOME/.nvm/versions/node/(node --version)/lib/node_modules


apt install -y python2
npm i -g yarn gulp



#? Needs to know where the modules are within node command line
