# Alliance DevLab
Alliance DevLab is devoted to serving the infrastructural needs of co-authoring communities.

## Setup Ubuntu for Local Development:
After a fresh install of Ubuntu simply:

```bash
wget -q -O - https://gitlab.com/alliance-devlab/setup/ubuntu/-/raw/main/Ansible.init | bash
```

### How to Run an Ansible playbook locally:

Source: https://gist.github.com/alces/caa3e7e5f46f9595f715f0f55eef65c1

* using Ansible command line:

```bash
ansible-playbook --connection=local 127.0.0.1 playbook.yml
```

* using playbook header:

```yaml
- hosts: 127.0.0.1
  connection: local
```

* using Ansible configuration file:

```ini
[defaults]
transport = local
```

* using inventory:

```ini
127.0.0.1 ansible_connection=local
```


<!--
#### Node development:
```bash
./ubuntu-setup/development/Node.setup.sh
```

#### PHP development:
```bash
./ubuntu-setup/development/PHP.setup.sh
```

#### Rust development:
```bash
./ubuntu-setup/development/Rust.setup.sh
```
-->
